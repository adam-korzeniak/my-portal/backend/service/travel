package com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Placement {

    private String name;
    private String description;
    private String styleUrl;
    private Point point;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Point {
        private String longitude;
        private String latitude;
    }
}
