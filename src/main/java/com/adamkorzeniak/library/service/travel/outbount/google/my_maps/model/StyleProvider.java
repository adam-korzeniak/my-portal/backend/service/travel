package com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model;

import com.adamkorzeniak.library.service.travel.location.model.Location;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

@Component
class StyleProvider {

    private final Map<String, String> colors = new HashMap<>();
    private final Map<String, String> icons = new HashMap<>();

    StyleProvider(){
        this.colors.put("default", "ffd18802");
        this.colors.put("blue", "ffd18802");
        this.colors.put("red", "ff0051e6");
        this.colors.put("dark blue", "ffab4939");
        this.colors.put("black", "ff000000");
        this.colors.put("violet", "ffb0279c");

        this.icons.put("pin", "1899");
        this.icons.put("food", "1577");
        this.icons.put("parking", "1644");
        this.icons.put("city", "1547");
    }

    private String getIcon(String name) {
       return this.icons.getOrDefault(name, "1899");
    }

    private String getColor(String name) {
        return this.colors.getOrDefault(name, "1899");
    }

    private String getColorId(String name) {
        String color = getColor(name);
        return color.substring(6, 8) + color.substring(4, 6) + color.substring(2, 4);
    }

    public String getStyleUrl(Location location) {
        String icon = getIcon("");
        String color = getColor("");
        return MessageFormat.format("#icon-{0}-{1}", icon, color);
    }
}
