package com.adamkorzeniak.library.service.travel.outbount.google.my_maps;

import com.adamkorzeniak.library.service.travel.legacy.SampleGenerator;
import com.adamkorzeniak.library.service.travel.location.model.Location;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.Item;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.MyMapsDocument;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class GoogleMyMapsGenerator {


    public static void main(String[] args) {
        MyMapsDocument myMapsDocument = SampleGenerator.generateSample();
        String result = new KmlGenerator().generateFile(myMapsDocument);
        System.out.println(result);
    }

    private KmlGenerator kmlGenerator;

    public void generateMyMapsExportFile(String title, List<Location> locations) {
        MyMapsDocument doc = new MyMapsDocument();
        doc.setName(title);
        doc.setItems(buildItems(locations));
        kmlGenerator.generateFile(doc);
    }

    private List<Item> buildItems(List<Location> locations) {
        List<Item> items = new ArrayList<>();
        items.addAll(buildStyles(locations));
        items.addAll(buildContent(locations));
        return items;
    }

    private List<Item> buildStyles(List<Location> locations) {
        return new ArrayList<>();
    }

    private List<Item> buildContent(List<Location> locations) {
        return new ArrayList<>();
    }
}
