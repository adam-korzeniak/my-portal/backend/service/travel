package com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model;

import lombok.Data;

import java.util.List;

@Data
public class MyMapsDocument {
    private String name;
    private String description;
    private List<Item> items;
}
