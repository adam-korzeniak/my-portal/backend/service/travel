package com.adamkorzeniak.library.service.travel.location.model;

import lombok.Data;

@Data
public class GoogleDetails {
    private Double rating;
    private Integer ratingCount;
}