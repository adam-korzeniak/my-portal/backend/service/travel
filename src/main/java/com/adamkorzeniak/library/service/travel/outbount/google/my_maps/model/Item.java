package com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model;

import lombok.Data;

@Data
public class Item {
    private String name;
    private String description;
}
