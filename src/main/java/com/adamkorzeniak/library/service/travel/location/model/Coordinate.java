package com.adamkorzeniak.library.service.travel.location.model;

import lombok.Data;

@Data
public class Coordinate {
    private double latitude;
    private double longitude;
}
