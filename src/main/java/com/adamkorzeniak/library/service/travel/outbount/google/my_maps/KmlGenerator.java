package com.adamkorzeniak.library.service.travel.outbount.google.my_maps;

import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.Folder;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.Item;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.MyMapsDocument;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.Placement;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.Style;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.StyleMap;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
class KmlGenerator {

    public String generateFile(MyMapsDocument myMapsDocument) {
        return new StringBuilder()
                .append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
                .append("<kml xmlns=\"http://www.opengis.net/kml/2.2\">")
                .append(buildTag("Document", generateContent(myMapsDocument)))
                .append("</kml>")
                .toString();
    }

    private StringBuilder generateContent(MyMapsDocument myMapsDocument) {
        return new StringBuilder()
                .append(buildTag("name", myMapsDocument.getName()))
                .append(buildTag("description", myMapsDocument.getDescription()))
                .append(generateItems(myMapsDocument.getItems()));
    }

    private StringBuilder generateItems(List<Item> items) {
        StringBuilder content = new StringBuilder();
        for (Item item: items) {
            if (item instanceof Style style) {
                content.append(buildTag("Style", generateStyle(style), Map.of("id", style.getId())));
            } else if (item instanceof StyleMap styleMap) {
                content.append(buildTag("StyleMap", generateStyleMap(styleMap), Map.of("id", styleMap.getId())));
            } else if (item instanceof Folder folder) {
                content.append(buildTag("Folder", generateFolder(folder)));
            }
        }

        return content;
    }

    private static StringBuilder generateStyle(Style style) {
        return new StringBuilder()
                .append(buildTag("IconStyle", generateIconStyle(style.getIconStyle())))
                .append(buildTag("LabelStyle", generateLabelStyle(style.getLabelStyle())))
                .append(buildTag("BalloonStyle", generateBalloonStyle(style.getBalloonStyle())));
    }

    private static CharSequence generateIconStyle(Style.IconStyle iconStyle) {
        return new StringBuilder()
                .append(buildTag("color", iconStyle.getColor()))
                .append(buildTag("scale", iconStyle.getScale()))
                .append(buildTag("scale", iconStyle.getScale()))
                .append(buildTag("hotspot", Map.of(
                        "x", "32",
                        "xunit", "pixels",
                        "y", "64",
                        "yunits", "insetPixels")));

    }

    private static CharSequence generateLabelStyle(Style.LabelStyle labelStyle) {
        return buildTag("scale", labelStyle.getScale());
    }
    private static CharSequence generateBalloonStyle(Style.BalloonStyle balloonStyle) {
        return buildTag("balloonStyle", balloonStyle.getText());
    }

    private StringBuilder generateStyleMap(StyleMap styleMap) {
        StringBuilder output = new StringBuilder();
        for (StyleMap.Pair pair: styleMap.getPairs()) {
                output.append(buildTag("Pair", generatePair(pair)));
        }
        return output;
    }

    private StringBuilder generatePair(StyleMap.Pair pair) {
        return new StringBuilder()
                .append(buildTag("key", pair.getKey()))
                .append(buildTag("styleUrl", pair.getStyleUrl()));
    }

    private static StringBuilder generateFolder(Folder folder) {
        StringBuilder content = new StringBuilder()
                .append(buildTag("name", folder.getName()))
                .append(buildTag("description", folder.getDescription()));

        for (Placement placement: folder.getPlacemarks()) {
            content.append(buildTag("Placemark", generatePlacemark(placement)));
        }
        return content;
    }

    private static StringBuilder generatePlacemark(Placement placement) {
        return new StringBuilder()
                .append(buildTag("name", placement.getName()))
                .append(buildTag("description", placement.getDescription()))
                .append(buildTag("Point", generatePoint(placement.getPoint())));
    }

    private static StringBuilder generatePoint(Placement.Point point) {
        StringBuilder coordinates = new StringBuilder()
                .append("\n")
                .append(point.getLongitude()).append(",")
                .append(point.getLatitude()).append(",")
                .append(0)
                .append("\n");
        return new StringBuilder()
                .append(buildTag("coordinates", coordinates));
    }

    private static StringBuilder buildTag(String tagName, Map<String, String> attributes) {
        return buildTag(tagName, null, Map.of());
    }

    private static StringBuilder buildTag(String tagName, int content) {
        return buildTag(tagName, Integer.toString(content), Map.of());
    }

    private static StringBuilder buildTag(String tagName, CharSequence content) {
        return buildTag(tagName, content, Map.of());
    }

    private static StringBuilder buildTag(String tagName, CharSequence content, Map<String, String> attributes) {
        if (content == null && attributes.isEmpty()) {
            return new StringBuilder();
        }
        StringBuilder output = new StringBuilder()
                .append("<").append(tagName);
        for (Map.Entry<String, String> entry: attributes.entrySet()) {
            output
                    .append(" ")
                    .append(entry.getKey())
                    .append("\"")
                    .append(entry.getValue())
                    .append("\"");
        }
        if (content == null) {
            return output.append("/>");
        }
        return output
                .append(">")
                .append(content)
                .append("</").append(tagName).append(">");
    }

}
