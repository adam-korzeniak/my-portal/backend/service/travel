package com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class StyleMap extends Item {
    private String id;
    private String name;
    private String description;
    private List<Pair> pairs;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class Pair {
        private String key;
        private String styleUrl;
    }
}
