package com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Folder extends Item {
    private String name;
    private List<Placement> placemarks;
}
