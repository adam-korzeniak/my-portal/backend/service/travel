package com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Style extends Item {
    private String id;
    private String name;
    private String description;
    private IconStyle iconStyle;
    private BalloonStyle balloonStyle;
    private LabelStyle labelStyle;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class IconStyle {
        private String color;
        private Integer scale;
        private Icon icon;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Icon {
        private String href;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LabelStyle {
        private Integer scale;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BalloonStyle {
        private String text;
    }
}
