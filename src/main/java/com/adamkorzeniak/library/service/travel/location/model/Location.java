package com.adamkorzeniak.library.service.travel.location.model;

import lombok.Data;

@Data
public class Location {
    private String name;
    private String description;
    private String type;
    private Integer priority;
    private GoogleDetails googleDetails;
    private Coordinate coordinates;
}
