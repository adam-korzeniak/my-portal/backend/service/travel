package com.adamkorzeniak.library.service.travel.legacy;

import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.Folder;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.Item;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.MyMapsDocument;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.Placement;
import com.adamkorzeniak.library.service.travel.outbount.google.my_maps.model.StyleProvider;

import java.util.ArrayList;
import java.util.List;

class SampleGenerator {

    private final StyleProvider styleProvider = new StyleProvider();
    static MyMapsDocument generateSample() {
        MyMapsDocument myMapsDocument = new MyMapsDocument();
        myMapsDocument.setName("Włochy");
        myMapsDocument.setDescription("Sample map");

        List<Item> items = new ArrayList<>();
        items.addAll(generateStyles());
        items.addAll(generateFolders());

        myMapsDocument.setItems(items);
        return myMapsDocument;
    }

    private static List<Item> generateStyles() {
        return new ArrayList<>();
    }

    private static List<Item> generateFolders() {
        List<Placement> loty = List.of(
                new Placement("Bari - Lotnisko", null, "", new Placement.Point("16.765202", "41.137508")),
                new Placement("Wenecja - Treviso - Lotnisko", "https://www.google.com/maps/search/Treviso+Lotnisko", "", new Placement.Point("12.1944795", "45.6498881")),
                new Placement("Olbia - Lotnisko", null, "", new Placement.Point("9.5166575", "40.8998858"))
        );
        List<Placement> wulkany = List.of(
                new Placement("Wezuwiusz - moja nazwa\n\nhhh", "<![CDATA[Mój opis<br><br>zzz]]>", "", new Placement.Point("14.4289058", "40.822383")),
                new Placement("Etna - xxx", "yyy", "", new Placement.Point("14.9934349", "37.751005"))
        );

        Folder folderLoty = new Folder("Loty", loty);
        Folder folderWulkany = new Folder("Wulkany", wulkany);
        return List.of(folderLoty, folderWulkany);
    }
}