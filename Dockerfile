FROM eclipse-temurin:17
MAINTAINER adamkorzeniak.com
COPY target/travel-*.jar travel.jar
EXPOSE 8443
ENTRYPOINT ["java","-jar","/travel.jar"]